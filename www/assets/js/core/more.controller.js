appFenden.controller('moreController',['$scope','$routeParams','$http','$location','$window',function($scope, $routeParams ,$http, $location,$window) {
		
    $http({
      method  : 'POST',
      url     : '/offerData',
      data : { 'id' : $routeParams.id }
     })
     .then(function successCallback(response) {
		var data = response.data;
     	if(data.success){
 			var url = 'https://www.facebook.com/sharer.php?caption='+data.data[0].title+'&description='+data.data[0].commentary+'&u=https://fendenapp.com/more/'+data.data[0].id+'&picture='+(data.data[0].thumbnail != undefined ? 'https://fendenapp.com/offerImage/'+data.data[0].id+'/0' : '');

     		$scope.title = data.data[0].title;
     		$scope.price = data.data[0].price;
     		$scope.shop = data.data[0].shop;
     		$scope.branch = data.data[0].branch;
     		$scope.address = data.data[0].address;
     		$scope.latitude = data.data[0].latitude;
     		$scope.longitude = data.data[0].longitude;
     		$scope.commentary = data.data[0].commentary;
     		$scope.user = { 
     				id : data.data[0].postedBy._id,
     				avatar : data.data[0].postedBy.profile_photo,
     				name : data.data[0].postedBy.first_name+(data.data[0].postedBy.last_name != undefined ? ' '+data.data[0].postedBy.last_name : '' ),
     		};
     		$scope.date = new Date(data.data[0].date).toISOString().slice(0,10);
     		$scope.date = data.data[0].date;
     		$scope.url_twitter = 'https://twitter.com/share?url=https://fendenapp.com/#/more/'+$routeParams.id+'&hashtags=Fenden&text='+data.data[0].commentary;
     		var listaImages = new Array;
     		for (var i=0; i < data.data[0].thumbnails.length; i++) {
			   listaImages.push({
			   		thumb : data.data[0].thumbnails[i],
			   		image : '/offerImage/'+$routeParams.id+'/'+i
			   });
			 };
     		$scope.thumbnails = listaImages;
     		
     		//Declaracion de funcion para compartir en facebook

     		$scope.share_facebook = function(){
     			$window.open(url, "popup", "width=300,height=200,left=10,top=150");
     		}
     		$scope.share_twitter = 'https://twitter.com/intent/tweet?text='+data.data[0].title+'&url=https://fendenapp.com/more/'+$routeParams.id;
     		$scope.share_google = 'https://plus.google.com/share?url=https://fendenapp.com/more/'+$routeParams.id;
     		
     	} else {
	 		swal("Ocurrio un error con el webservice!");
     	}
     });
	 
	 
	 $scope.comments = new Array;
	 
	 $http({
      method  : 'POST',
      url     : '/listComments',
      data : { 'id' : $routeParams.id }
     })
     .then(function successCallback(response) {
		var data = response.data;
     	for (var i=0; i < data.data.length; i++) {
     		var arr_date = data.data[i].date.split('.');
		 	$scope.comments.push({ 
		 		id : data.data[i].id,
		 		avatar : data.data[i].postedBy.profile_photo,
		 		name : data.data[i].postedBy.first_name+((data.data[i].postedBy.last_name != undefined) ? ' '+data.data[i].postedBy.last_name : '' ),
		 		thumb : (data.data[i].thumb != undefined ? data.data[i].thumb : false),
		 		commentary : data.data[i].commentary,
		 		date : arr_date[0].replace('T',' ')
		 	});
     	}
     });
	 
	 $scope.textComment = '';
	 $scope.addComment = function(){
	   	$http({
	      method  : 'POST',
	      url     : '/addComment',
	      data : { 
	    	  	'id' : $routeParams.id, 
	    	  	'commentary' : $scope.textComment,
	    	  	'image' : angular.element('#imageInputComment').val(),
	    	  	'thumb' : angular.element('#imageInputCommentThumb').val() 
	      },
	      headers : {'Content-Type': 'application/json'} 
	    })
	    .then(function successCallback(response) {
			var data = response.data;
	 		$scope.textComment = '';
	 		if(data.success){
			 	$scope.comments.unshift({ 
			 		id: data.data.id,
			 		avatar : data.data.postedBy.profile_photo,
			 		name : data.data.postedBy.first_name+((data.data.postedBy.last_name != undefined) ? ' '+data.data.postedBy.last_name : '' ),
			 		commentary: data.data.commentary
			 	});
			 	
	 		} else {
		 		swal("Ocurrio un error al agregar el comentario");
	 		}
	    });
		
	 }
	 
	 
	 $scope.mark = function(){
		 $http({
		      method  : 'POST',
		      url     : '/mark',
		      data : { 'id' : $routeParams.id },
		      headers : {'Content-Type': 'application/json'} 
		 }).then(function successCallback(response) {
			
		 });
	 }
}]);