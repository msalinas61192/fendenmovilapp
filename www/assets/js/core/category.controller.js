appFenden.controller('categoryController',['$scope','$routeParams','$http','$location',function($scope, $routeParams, $http, $location) {
	$http({
      method  : 'POST',
      url     : '/infoCategory',
      data : { nombre : $routeParams.nombre }
	}).then(function successCallback(response) {
		var data = response.data;
 		if(data.success){
		 	$scope.title_category = data.data[0].category;
		 	$scope.classes = data.data[0].classes;

		 	$scope.listOffer = new Array;
			$http({
		      method  : 'POST',
		      url     : '/listCategoryOffers',
		      data	  : { id : data.data[0]._id }
		     }).then(function successCallback(response) {
				var data = response.data;
		     	if(data.success){
		     		for (var i=0; i < data.data.length; i++) {
					 	$scope.listOffer.push({ 
					 		id : data.data[i].id,
					 		title : data.data[i].title,
							price : data.data[i].price,
							shop : data.data[i].shop,
							branch : data.data[i].branch,
							start_date : new Date(data.data[i].start_date).toISOString().slice(0,10) ,
							end_date : new Date(data.data[i].end_date).toISOString().slice(0,10),
							commentary : data.data[i].commentary,
							address : data.data[i].address,
							image: data.data[i].thumbnail,
					 		profile_id: data.data[i].postedBy._id,
					 		avatar: data.data[i].postedBy.profile_photo,
					 		user_id: data.data[i].postedBy._id,
					 		name: data.data[i].postedBy.first_name+' '+data.data[i].postedBy.last_name,
					 		category : data.data[i].category
					 	});
					}
		     	}
		     });
 		} else {
	 		swal("Ocurrio un error al obtener informacion de la caegoria");
 		}
	});
    //console.log($routeParams.nombre);
}]);
