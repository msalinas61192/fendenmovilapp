import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera } from '@ionic-native/camera';
import { IonicStorageModule } from '@ionic/storage';
import { GoogleMaps } from '@ionic-native/google-maps';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { CategoryPage } from '../pages/category/category';
import { NearbyPage } from '../pages/nearby/nearby';
import { OfferupPage } from '../pages/offerup/offerup';
import { OfferPage } from '../pages/offer/offer';
import { CommentsPage } from '../pages/comments/comments';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
	DashboardPage,
	CategoryPage,
	NearbyPage,
	OfferupPage,
	OfferPage,
    CommentsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
	DashboardPage,
	CategoryPage,
	NearbyPage,
	OfferupPage,
	OfferPage,
    CommentsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
	InAppBrowser,
	Camera,
	GoogleMaps,
    Geolocation,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
