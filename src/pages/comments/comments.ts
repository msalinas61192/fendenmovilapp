import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular';
import { OfferPage } from '../offer/offer';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the CommentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-comments',
  templateUrl: 'comments.html',
})
export class CommentsPage {
  id_offer : any;
  data: any[] = [];
  dashboardPage = DashboardPage;
  user_id: any;
  
  first_name: any;
  last_name: any;
  profile_photo: any;
  
  comentario: any;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http,
              private storage: Storage,
              public alertCtrl: AlertController
             ) {}
  
  addComment(){
	let alertaComentarios = this.alertCtrl.create({
	  title: 'Opps!',
	  subTitle: 'Ocurrio un error al agregar el comentario',
	  buttons: ['Aceptar']
	});	  
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let data_comment = { id: this.id_offer, commentary: this.comentario, user_id: this.user_id };
    this.http.post('https://fendenapp.com/addComment',data_comment, options).map(res => res.json()).subscribe(data => {
        if(data.success){
            this.data.unshift({
                   'commentary' : data.data.commentary,
                   'date' : '',
                   'nombre' : this.first_name+' '+this.last_name,
                   'profile_photo' : data.data.postedBy.profile_photo
            });
        } else {
            alertaComentarios.present();
        }
    },err => {
        alertaComentarios.present();
    });
    this.comentario = '';
  }
  
  goToOffer(){
    this.navCtrl.push(OfferPage, {
        offer: this.id_offer
    });
  }
  
  ionViewDidLoad() {
    this.id_offer = this.navParams.get('offer');
    
    this.storage.get('user_id').then((user_id) => {
        this.user_id = user_id;
    });
    
    this.storage.get('first_name').then((first_name) => {
        this.first_name = first_name;
    });
    
    this.storage.get('last_name').then((last_name) => {
        this.last_name = last_name;
    });
    
    this.storage.get('profile_photo').then((profile_photo) => {
        this.profile_photo = profile_photo;
    });
      
    //Generar alerta carga publicaciones
	let alertaComentarios = this.alertCtrl.create({
	  title: 'Opps!',
	  subTitle: 'Ocurrio un error al cargar los comentarios',
	  buttons: ['Aceptar']
	});	  
      
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.http.post('https://fendenapp.com/listComments',{ id : this.id_offer }, options).map(res => res.json()).subscribe(data => {
        if(data.success){
           for (let item of data.data) {
                let nombre = '';
                let profile_photo = '';
                
                if(item.postedBy != null){
                    nombre += (item.postedBy.first_name != null?item.postedBy.first_name:'');
                    nombre += ' ';
                    nombre += (item.postedBy.last_name != null?item.postedBy.last_name:'');
                    profile_photo = item.postedBy.profile_photo;
                }
                this.data.push({
                   'commentary' : item.commentary,
                   'date' : item.date.split('.')[0].replace('T',' '),
                   'nombre' : nombre,
                   'profile_photo' : profile_photo
                });
           }
        } else {
            alertaComentarios.present();
        }
    },err => {
        alertaComentarios.present();
    });
  }
  
}
