import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OfferupPage } from './offerup';

@NgModule({
  declarations: [
    OfferupPage,
  ],
  imports: [
    IonicPageModule.forChild(OfferupPage),
  ],
  exports: [
    OfferupPage
  ]
})
export class OfferupPageModule {}
