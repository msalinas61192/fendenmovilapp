import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { DashboardPage } from '../dashboard/dashboard';
import { OfferPage } from '../offer/offer';
import { CommentsPage } from '../comments/comments';


import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-offerup',
  templateUrl: 'offerup.html',
})
export class OfferupPage {

  dashboardPage = DashboardPage;
  commentsPage = CommentsPage;
  categories: any;
  listImages: any[] = [];
  listImagesThumbs: any[] = [];
  

  
  /*
  * Variables de formulario,
  * se utilizan para validacion de datos
  * y luego se envian a webservice de sitio web
  * FendenApp.com
  */
  ofertaTitulo: any;
  ofertaCategoria: any;
  ofertaPrecio: any;
  ofertaTienda: any;
  ofertaSucursal: any;
  ofertaInicio: any;
  ofertaFin: any;
  ofertaComentario: any;
  ofertaLatitude: any;
  ofertaLongitude: any;
  ofertaUbicacion: any;
  user_id: any;
  
  
  //Variable busqueda de direccion
  textoDireccion: any;
  
  constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				private camera: Camera,
				private renderer: Renderer, 
				public http: Http,
				public geolocation: Geolocation,
                private storage: Storage,
				private googleMaps: GoogleMaps,
				public alertCtrl: AlertController
             ) {}
  
  @ViewChild('step1') step1:ElementRef;
  @ViewChild('step2') step2:ElementRef;
  @ViewChild('step3') step3:ElementRef;
  @ViewChild('step4') step4:ElementRef;
  
  @ViewChild('alerttitulo') alerttitulo:ElementRef;
  @ViewChild('alertcategoria') alertcategoria:ElementRef;
  @ViewChild('alertprecio') alertprecio:ElementRef;
  @ViewChild('alerttienda') alerttienda:ElementRef;
  @ViewChild('alertsucursal') alertsucursal:ElementRef;
  @ViewChild('alertinicio')alertinicio:ElementRef;
  @ViewChild('alertfin') alertfin:ElementRef;
  @ViewChild('alertcomentario') alertcomentario:ElementRef;
  
  @ViewChild('takephoto') takephoto:ElementRef;
  

  buscarMapa(){  
	this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+this.textoDireccion).map(res => res.json()).subscribe(data => {
		if(data.results.length > 0){
			let cords = {
				latitude : data.results[0].geometry.location.lat,
				longitude : data.results[0].geometry.location.lng
			};
			this.loadMap(cords,this.textoDireccion);
		}
	},error => {
		console.log(error);
	});
  }
  
  showStep(numParam: number){
	switch (numParam) { 
		case 1: 
		  this.renderer.setElementClass(this.step1.nativeElement,'oculto', false);
		  this.renderer.setElementClass(this.step2.nativeElement,'oculto', true);
		  this.renderer.setElementClass(this.step3.nativeElement,'oculto', true);
		  break; 
		case 2: 
		  let datosStep1 = {
			ofertaTitulo : this.ofertaTitulo,
			ofertaCategoria : this.ofertaCategoria,
			ofertaPrecio : this.ofertaPrecio,
			ofertaTienda : this.ofertaTienda,
			ofertaSucursal : this.ofertaSucursal,
			ofertaInicio : this.ofertaInicio,
			ofertaFin : this.ofertaFin,
			ofertaComentario : this.ofertaComentario
		  };
		  
		  let error = false;
		  /*
		  * Validacion de titulo
		  */  
		  if(this.ofertaTitulo == undefined || this.ofertaTitulo.length < 6) {
			error = true;
			this.renderer.setElementClass(this.alerttitulo.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alerttitulo.nativeElement,'oculto', true);
		  }
		  
		  /*
		  * Validacion de categoria
		  */
		  if(this.ofertaCategoria == undefined) {
			error = true;
			this.renderer.setElementClass(this.alertcategoria.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertcategoria.nativeElement,'oculto', true);
		  }
		  
		  /*
		  * Validacion de precio
		  */
		  if(Number(this.ofertaPrecio) == NaN || this.ofertaPrecio == undefined) {
			error = true;
			this.renderer.setElementClass(this.alertprecio.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertprecio.nativeElement,'oculto', true);
		  }
		  
		  /*
		  * Validacion de tienda
		  */
		  if(this.ofertaTienda == undefined) {
			error = true;
			this.renderer.setElementClass(this.alerttienda.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alerttienda.nativeElement,'oculto', true);
		  }
		  
		  /*
		  * Validacion de sucursal
		  */
		  if(this.ofertaSucursal == undefined) {
			error = true;
			this.renderer.setElementClass(this.alertsucursal.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertsucursal.nativeElement,'oculto', true);
		  }
		  
		  
		  /*
		  * Validacion de fecha de inicio
		  */
		  if(this.ofertaInicio == undefined) {
			error = true;
			this.renderer.setElementClass(this.alertinicio.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertinicio.nativeElement,'oculto', true);
		  }
		  
		  
		   
		  /*
		  * Validacion de fecha de fin
		  */
		  if(this.ofertaFin == undefined) {
			error = true;
			this.renderer.setElementClass(this.alertfin.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertfin.nativeElement,'oculto', true);
		  }
		  
		  
		  if(this.ofertaComentario == undefined){
			error = true;
			this.renderer.setElementClass(this.alertcomentario.nativeElement,'oculto', false);
		  } else {
			this.renderer.setElementClass(this.alertcomentario.nativeElement,'oculto', true);
		  }
		  
		  if(error){
			  console.log('Ocurrio error');
		  } else {
			this.renderer.setElementClass(this.step1.nativeElement,'oculto', true);
			this.renderer.setElementClass(this.step2.nativeElement,'oculto', false);
			this.renderer.setElementClass(this.step3.nativeElement,'oculto', true);
		  }
		  break;
		case 3: 
            
          if(this.listImages.length >= 1){
            this.renderer.setElementClass(this.step1.nativeElement,'oculto', true);
            this.renderer.setElementClass(this.step2.nativeElement,'oculto', true);
            this.renderer.setElementClass(this.step3.nativeElement,'oculto', false);
            //Cargar mapa con ubicacion de GPS
            this.geolocation.getCurrentPosition().then((resp) => {
                this.loadMap(resp.coords,'Tú Ubicación');
            }).catch((error) => {
                console.log('Error getting location', error);
            });
          } else {
            let alert = this.alertCtrl.create({
                title: 'Advertencia',
                subTitle: 'Debe tomar al menos una fotografía',
                buttons: ['Aceptar']
            });
            alert.present();
          }
		  break; 
		case 4:
            
            this.renderer.setElementClass(this.step1.nativeElement,'oculto', true);
            this.renderer.setElementClass(this.step2.nativeElement,'oculto', true);
            this.renderer.setElementClass(this.step3.nativeElement,'oculto', true);
            this.renderer.setElementClass(this.step4.nativeElement,'oculto', false);
            let datosOferta = {
				ofertaTitulo : this.ofertaTitulo,
				ofertaPrecio : this.ofertaPrecio,
				ofertaTienda : this.ofertaTienda,
				ofertaSucursal : this.ofertaSucursal,
				ofertaInicio : this.ofertaInicio,
				ofertaFin : this.ofertaFin,
				ofertaComentario : this.ofertaComentario,
				ofertaUbicacion : this.ofertaUbicacion,
				thumb1 : this.listImagesThumbs[0],
				thumb2 : this.listImagesThumbs[1],
				thumb3 : this.listImagesThumbs[2],
				imagen1 : this.listImages[0],
				imagen2 : this.listImages[1],
				imagen3 : this.listImages[2],
				category : this.ofertaCategoria,
				latitude : parseFloat(this.ofertaLatitude),
				longitude : parseFloat(this.ofertaLongitude),
				user_id : this.user_id
			}; 
        
            
            let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Ocurrio un error inesperado al ingresar la oferta',
                buttons: ['Aceptar']
            });
            
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            this.http.post('https://fendenapp.com/offerup',datosOferta, options).map(res => res.json()).subscribe(data => {
                if(data.success){
                    this.goToOffer(data.id);
                } else {
                    alert.present();
                }
            },err => {
                alert.present();
            });
          break;
    }   
  }
  
  goToOffer(offer: string){
    this.navCtrl.push(OfferPage, {
        offer: offer
    });
  }
  
  goToComments(offer: string){
    this.navCtrl.push(CommentsPage, {
        offer: offer
    });
  }
  
  
  ionViewDidLoad() {
	this.storage.get('menu').then(data => {
	   this.categories = data;
	});
    
    this.storage.get('user_id').then((user_id) => {
        this.user_id = user_id;
    });
    
    //Volver al dashboard
    document.addEventListener('backbutton', () => {
        if(this.navCtrl.canGoBack()){
            console.log('Se puede volver atraz');
        } else {
            console.log('No se puede volver atraz');
        }
    });
  }
  
 

  
  loadMap(resp: any, ubicacion: string) {
    this.ofertaLatitude = resp.latitude;
    this.ofertaLongitude = resp.longitude;
    
    this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+resp.latitude+','+resp.longitude+'&sensor=false').map(res => res.json()).subscribe(data => {
		this.ofertaUbicacion = data.results[0].formatted_address;
	},error => {
		console.log(error);
	});
    
	let element: HTMLElement = document.getElementById('map2');
	let map: GoogleMap = this.googleMaps.create(element);

	map.one(GoogleMapsEvent.MAP_READY).then(() => {
		let ionic: LatLng = new LatLng(resp.latitude,resp.longitude);
		let position: CameraPosition = {
			target: ionic,
			zoom: 15,
			tilt: 30
		};
		map.moveCamera(position);
		
		let markerOptions: MarkerOptions = {
		   position: ionic,
		   icon: 'www/assets/img/maps/blue-icon.png',
		   title: ubicacion
		};
		
		map.addMarker(markerOptions).then((marker: Marker) => {
			marker.showInfoWindow();
		});
	});
  }
  
  convertToBase64(url, outputFormat) {
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        
        if(img.width > 1000){
            canvas.width = img.width*(1000/img.width);
            canvas.height = img.height*(1000/img.width);
        } else {
            if(img.height > 1000){
                canvas.width = img.width*(1000/img.height);
                canvas.height = img.height*(1000/img.height);
            } else {
                canvas.height = img.height;
                canvas.width = img.width;
            }
        }
          
        ctx.drawImage(img, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
  
  
   convertToBase64Thumbs(url, outputFormat) {
     return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement>document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        
        canvas.width = 100;
        canvas.height = 100;
          
        ctx.drawImage(img, 0, 0, 100, 100);

        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
   }
  
  getPicture(){
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1000,
      targetHeight: 1000,
      quality: 100
    }
    this.camera.getPicture( options )
    .then(imageData => {
       
        let image = `data:image/jpeg;base64,${imageData}`;
        
        this.convertToBase64(image,'image/jpg').then((data) => {
            this.listImages.push(data);
        });
        
        this.convertToBase64Thumbs(image,'image/jpg').then((data) => {
            this.listImagesThumbs.push(data);
        });
        
        if(this.listImages.length >= 3){
            this.renderer.setElementClass(this.takephoto.nativeElement,'oculto', true);
        }
    }).catch(error =>{
      console.error( error );
    });
  }
}
