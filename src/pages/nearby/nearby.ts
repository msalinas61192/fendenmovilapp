import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Geolocation } from '@ionic-native/geolocation';
import { Platform } from 'ionic-angular';

import { DashboardPage } from '../dashboard/dashboard';
import { OfferPage } from '../offer/offer';

import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';


declare var google: any;

@IonicPage()
@Component({
  selector: 'page-nearby',
  templateUrl: 'nearby.html',
})
export class NearbyPage {
  dashboardPage = DashboardPage;
  
  constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public geolocation: Geolocation,
				public platform: Platform,
                public http: Http,
				private googleMaps: GoogleMaps) {}

  ionViewDidLoad(){
    //Volver al dashboard
    document.addEventListener('backbutton', () => {
        if(this.navCtrl.canGoBack()){
            console.log('Se puede volver atraz');
        } else {
            console.log('No se puede volver atraz');
        }
    });
  }
  
  ngAfterViewInit(){
	this.loadMap();
  }

  loadMap() {
	this.geolocation.getCurrentPosition().then((resp) => {
		let element: HTMLElement = document.getElementById('map');
		let map: GoogleMap = this.googleMaps.create(element);

		map.one(GoogleMapsEvent.MAP_READY).then(() => {
			let ionic: LatLng = new LatLng(resp.coords.latitude,resp.coords.longitude);
			let position: CameraPosition = {
				target: ionic,
				zoom: 15,
				tilt: 30
			};
			map.moveCamera(position);
			
			let markerOptions: MarkerOptions = {
				position: ionic,
				title: 'Tú Ubicación',
				icon: 'www/assets/img/maps/me-icon.png'
			};
			
			map.addMarker(markerOptions).then((marker: Marker) => {
				marker.showInfoWindow();
			});
			
            let cordenadas = { lat: resp.coords.latitude, lng: resp.coords.longitude };
            
            let headers = new Headers({ 'Content-Type': 'application/json' });
            let options = new RequestOptions({ headers: headers });
            this.http.post('https://fendenapp.com/listOffersNearby',cordenadas,options).map(res => res.json()).subscribe(data => {
                for(let i=0; i < data.length; i++){
                    let cordsPoint: LatLng = new LatLng(data[i].latitude,data[i].longitude);
					
                    let markerOptions: MarkerOptions = {
                        position: cordsPoint,
                        title: data[i].title,
                        icon: 'www/assets/img/maps/blue-icon.png',
						infoClick: () => {
							this.navCtrl.push(OfferPage, {
								offer: data[i]._id
							});
							console.log('Click');
						}
                    };
                    map.addMarker(markerOptions).then((marker: Marker) => {
                        marker.showInfoWindow();
                    });
                }
                console.log(data);
            }, error => {
                console.log(error);
            });
		});

	}).catch((error) => {
	  console.log('Error getting location', error);
	});
  }
  
  


}
