import { Component, ViewChild, ElementRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { CategoryPage } from '../category/category';
import { NearbyPage } from '../nearby/nearby';
import { OfferupPage } from '../offerup/offerup';
import { OfferPage } from '../offer/offer';
import { HomePage } from '../home/home';

/**
 * Generated class for the DashboardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
 
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  nearbyPage = NearbyPage;
  offerupPage = OfferupPage;	  
  categoryPage = CategoryPage;
  categories: any;
  offers: any;

  constructor(public navCtrl: NavController, 
				public navParams: NavParams,
				private renderer: Renderer, 
                public http: Http,
                private storage: Storage,
                public alertCtrl: AlertController,
                private platform: Platform
             ) {}
                
  @ViewChild('sidebar') sidebar:ElementRef;
  

  ionViewDidLoad(){
	this.storage.get('menu').then(data => {
	   this.categories = data;
	});
	
	//Generar alerta carga publicaciones
	let alertaPublicaciones = this.alertCtrl.create({
	  title: 'Opps!',
	  subTitle: 'Ocurrio un error al cargar las publicaciones',
	  buttons: ['Aceptar']
	});	
	
	//Cargar publicaciones base
	let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
	this.http.post('https://fendenapp.com/listOffers',{}, options).map(res => res.json()).subscribe(data => {
        if(data.success){
            this.offers = data.data;
            console.log(this.offers);
        } else {
            alertaPublicaciones.present();
        }
	},err => {
		alertaPublicaciones.present();
	});
    
    
    //Volver al dashboard
    document.addEventListener('backbutton', () => {
        if(this.navCtrl.canGoBack()){
            this.platform.exitApp();
        } else {
            console.log('No se puede volver atraz');
        }
    });
  }
  
  /*
  * Esta funcion permite cerrar sesion, luego de limpiar el 
  * objeto de sesion de localstorage redirecciona a HomePage
  */
  logout(){
    this.storage.clear(); 
    this.navCtrl.push(HomePage);
  }
  
  //Envia al usuario a la categoria seleccionada
  goToCategory(category: string){
    this.navCtrl.push(CategoryPage, {
        category: category
    });
  }
  
  goToOffer(offer: string){
    this.navCtrl.push(OfferPage, {
        offer: offer
    });
  }
  
  //Muestra y oculta el menu de usuario
  showMenu(){
	if(this.sidebar.nativeElement.classList.contains('menu-activo')) {
		this.renderer.setElementClass(this.sidebar.nativeElement,'menu-activo', false);
	} else {
		this.renderer.setElementClass(this.sidebar.nativeElement,'menu-activo', true);
	}
  }

}
