import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';

/**
 * Generated class for the CategoryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  dashboardPage = DashboardPage;
  public title_category;
  constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private platform: Platform) {
	  this.title_category = navParams.get('category');
	  console.log(this.title_category);
  }

  ionViewDidLoad() {
    //Volver al dashboard
    document.addEventListener('backbutton', () => {
        if(this.navCtrl.canGoBack()){
            console.log('Se puede volver atraz');
        } else {
            console.log('No se puede volver atraz');
        }
    });
  }

}
