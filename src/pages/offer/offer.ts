import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DashboardPage } from '../dashboard/dashboard';
import { CommentsPage } from '../comments/comments';


import { Geolocation } from '@ionic-native/geolocation';

import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker
} from '@ionic-native/google-maps';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-offer',
  templateUrl: 'offer.html',
})
export class OfferPage {
  dashboardPage = DashboardPage;
  id_offer: any;
  title: any;
  thumbnail: any;
  commentary: any;
  shop: any;
  branch: any;
  browser: any;
  user_id: any;
  
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http: Http,
              private googleMaps: GoogleMaps,
              private iab: InAppBrowser,
              private storage: Storage,
              public geolocation: Geolocation,
			  public platform: Platform) {}

  
  
  showComments(){
    this.navCtrl.push(CommentsPage, {
        offer: this.id_offer
    });
  }
  
  share_facebook(){
    let url = 'https://www.facebook.com/sharer.php?caption='+this.title+'&description='+this.commentary+'&u=https://fendenapp.com/more/'+this.id_offer+'&picture='+(this.thumbnail != undefined ? 'https://fendenapp.com/offerImage/'+this.id_offer+'/0' : '');
    this.browser = this.iab.create(url, '_self','location=no');
    
    this.browser.on('loadstart').subscribe((event: any) => {
        if(event.url.substring(0,44) == 'https://www.facebook.com/dialog/return/close'){
            this.browser.close();
        }
    });
    
  }
  
  share_twitter(){
    let url = 'https://twitter.com/intent/tweet?text='+this.commentary+'&url=https://fendenapp.com/more/'+this.id_offer;
    this.browser = this.iab.create(url, '_self','location=no');
   
    
    this.browser.on('loadstart').subscribe((event: any) => {
        if(event.url.substring(0,41) == 'https://twitter.com/intent/tweet/complete'){
            this.browser.close();
        }
    });
  }
  
  share_google(){
    let url = 'https://plus.google.com/share?url=https://fendenapp.com/more/'+this.id_offer;
    this.browser = this.iab.create(url, '_self','location=no');
    
    this.browser.on('loadstop').subscribe((event: any) => {
       console.log(event.url);
    });
    
    
    this.browser.on('loadstop').subscribe((event: any) => {
       console.log(event.url);
    });
  }
  
  ionViewWillEnter(){ 
    this.id_offer = this.navParams.get('offer');
    
    this.storage.get('user_id').then((user_id) => {
        this.user_id = user_id;
    });
  	  
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    this.http.post('https://fendenapp.com/offerData',{ id : this.id_offer }, options).map(res => res.json()).subscribe(data => {
        if(data.success){
            this.title = data.data[0].title;
            this.commentary = data.data[0].commentary; 
            this.thumbnail = data.data[0].thumbnails[0];
            
            this.shop = data.data[0].shop;
            this.branch = data.data[0].branch;
            
            this.loadMap({ latitude : data.data[0].latitude , longitude : data.data[0].longitude });
        }
        
        console.log(data.data[0]);
    },err => {
        console.log(err);
    });
  }
  
  like(type: any){
    let params = { 'type' : type, 'post_id' : this.id_offer, user_id: this.user_id };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    console.log(params);
    this.http.post('https://fendenapp.com/like',params, options).map(res => res.json()).subscribe(data => {
        console.log(data);
    },err => {
        console.log(err);
    });
  }
  
  loadMap(cords: any) {
    let element: HTMLElement = document.getElementById('map');
    let map: GoogleMap = this.googleMaps.create(element);
    console.log(cords);
    map.one(GoogleMapsEvent.MAP_READY).then(() => {
        console.log('Maps Ready');
        let ionic: LatLng = new LatLng(cords.latitude,cords.longitude);
        let position: CameraPosition = {
            target: ionic,
            zoom: 15,
            tilt: 30
        };
        map.moveCamera(position);
        
        let markerOptions: MarkerOptions = {
            position: ionic,
            title: this.title,
            icon: 'www/assets/img/maps/blue-icon.png'
        };
        map.addMarker(markerOptions).then((marker: Marker) => {
            marker.showInfoWindow();
        });
    }).catch((error) => {
	  console.log(error);
	});
  }

}
