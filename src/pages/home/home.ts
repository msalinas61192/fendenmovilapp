import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AlertController } from 'ionic-angular';

import { DashboardPage } from '../dashboard/dashboard';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  dashboardPage = DashboardPage;
  browser;
  constructor(public navCtrl: NavController,
              public platform: Platform,
              private iab: InAppBrowser,
              private storage: Storage,
			  public http: Http,
			  public alertCtrl: AlertController) {
    this.storage.get('user_id').then((user_id) => {
        if(user_id){
            this.navCtrl.push(DashboardPage);
        }
    });
	
	let alert = this.alertCtrl.create({
	  title: 'Opps!',
	  subTitle: 'Ocurrio un error al generar el menu',
	  buttons: ['OK']
	});	
	
	this.http.get('https://fendenapp.com/listCategories').map(res => res.json()).subscribe(data => {
		if(data.success){
			this.storage.set('menu', data.data);			
		} else {
			alert.present();
		}
	},err => {
		alert.present();
	});
  }
  
  ionViewDidLoad(){
    this.storage.get('user_id').then((user_id) => {
        if(user_id){
            this.navCtrl.push(DashboardPage);
        }
    });
  }
  
  /*
  * Login con Facebook, twitter y google recuperan datos json
  */
  loginFacebook(){
	  
	let browser = this.iab.create('https://fendenapp.com/auth/facebook', '_self','location=no');
	
	browser.on('loadstart').subscribe((event: any) => {
        if(event.url.substring(0,44) == 'https://fendenapp.com/auth/facebook/callback'){
			browser.hide();
		}
    });
    
    browser.on('loadstop').subscribe((event: any) => {
        if(event.url.substring(0,44) == 'https://fendenapp.com/auth/facebook/callback'){
            browser.executeScript({ code: 'document.body.innerHTML;' }).then((body)=> { 
                var datos_sesion = JSON.parse(body);
				console.log(datos_sesion);
                this.storage.set('user_id',datos_sesion.user_id);
                this.storage.set('facebook_id', datos_sesion.facebook_id);
                this.storage.set('first_name',datos_sesion.first_name);
                this.storage.set('last_name',datos_sesion.last_name);
                this.storage.set('profile_photo',datos_sesion.profile_photo);
                this.storage.set('rank',datos_sesion.rank);
				this.navCtrl.push(DashboardPage);
            });
        }
    });
  }
  
  loginTwitter(){
	  
	let browser = this.iab.create('https://fendenapp.com/auth/twitter', '_self','location=no');
	
	browser.on('loadstart').subscribe((event: any) => {
        if(event.url.substring(0,43) == 'https://fendenapp.com/auth/twitter/callback'){
			browser.hide();
		}
    });
    
    browser.on('loadstop').subscribe((event: any) => {
        if(event.url.substring(0,43) == 'https://fendenapp.com/auth/twitter/callback'){
            browser.executeScript({ code: 'document.body.innerHTML;' }).then((body)=> { 
                var datos_sesion = JSON.parse(body);
				console.log(datos_sesion);
                this.storage.set('user_id',datos_sesion.user_id);
                this.storage.set('twitter_id', datos_sesion.twitter_id);
                this.storage.set('first_name',datos_sesion.first_name);
                this.storage.set('last_name',datos_sesion.last_name);
                this.storage.set('profile_photo',datos_sesion.profile_photo);
                this.storage.set('rank',datos_sesion.rank);
				this.navCtrl.push(DashboardPage);
            });
        }
    });
  }
  
  loginGoogle(){
	  
	let browser = this.iab.create('https://fendenapp.com/auth/google', '_self','location=no');
	
	browser.on('loadstart').subscribe((event: any) => {
		if(event.url.substring(0,42) == 'https://fendenapp.com/auth/google/callback'){
		    browser.hide();
		}
    });
    
    browser.on('loadstop').subscribe((event: any) => {
        if(event.url.substring(0,42) == 'https://fendenapp.com/auth/google/callback'){
            browser.executeScript({ code: 'document.body.innerHTML;' }).then((body)=>{ 
                var datos_sesion = JSON.parse(body);
				console.log(datos_sesion);
                this.storage.set('user_id',datos_sesion.user_id);
                this.storage.set('google_id', datos_sesion.google_id);
                this.storage.set('first_name',datos_sesion.first_name);
                this.storage.set('last_name',datos_sesion.last_name);
                this.storage.set('profile_photo',datos_sesion.profile_photo);
                this.storage.set('rank',datos_sesion.rank);
				this.navCtrl.push(DashboardPage);
            });
        }
    });
  }

}
